import { Component } from '@angular/core';
import { CurrencyService } from './services/Currency.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public currency$ = this.currencyService.getCurrency$();

  constructor(
    private currencyService: CurrencyService
  ) {}

}
