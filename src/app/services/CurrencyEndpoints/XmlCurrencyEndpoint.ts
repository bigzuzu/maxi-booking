import { HttpClient } from '@angular/common/http';
import { ICurrencyEndpoint } from './CurrencyEndpoint.inteface';
import { map } from 'rxjs/operators';
import * as parser from 'fast-xml-parser'

export class XmlCurrencyEndpoint implements ICurrencyEndpoint {

    public url: string =  'https://www.cbr-xml-daily.ru/daily_utf8.xml';


    constructor(
        private _http: HttpClient
    ) {

    }

    getCurrency() {
        return this._http.get(this.url, { responseType: 'text' }).pipe(
            map((xmlstr) => {
                let response: any = parser.parse(xmlstr);
                return response.ValCurs.Valute;
            })
        );
    }
}