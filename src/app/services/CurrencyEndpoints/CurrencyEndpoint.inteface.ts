import { Observable } from 'rxjs';


export interface ICurrencyEndpoint {
    url: string;
    getCurrency: () => Observable<number>; 
}