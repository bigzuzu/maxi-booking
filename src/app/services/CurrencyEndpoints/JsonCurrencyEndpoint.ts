import { HttpClient } from '@angular/common/http';

import { ICurrencyEndpoint } from './CurrencyEndpoint.inteface';
import { map } from 'rxjs/operators';

export interface ICurrency {
    CharCode: string;
    ID: string;
    Name: string;
    Nominal: number;
    NumCode: string;
    Previous: number;
    Value: number;
}

export interface ICurrencyResponse {
    Date: string;
    PreviousDate: string;
    PreviousURL: string;
    Timestamp: string;
    Valute: {
        [key: string]: ICurrency
    }
}


export class JsonCurrencyEndpoint implements ICurrencyEndpoint {

    public url: string =  'https://www.cbr-xml-daily.ru/daily_json.js';


    constructor(
        private _http: HttpClient
    ) {

    }

    getCurrency() {
        return this._http.get<ICurrencyResponse>(this.url).pipe(
            map((response) => response.Valute['EUR'].Value)
        )
    }
}