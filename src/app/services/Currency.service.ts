import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { interval, BehaviorSubject, EMPTY, throwError } from 'rxjs'; 
import { map, switchMap, startWith, catchError } from 'rxjs/operators';
import { ICurrencyEndpoint } from './CurrencyEndpoints/CurrencyEndpoint.inteface';
import { JsonCurrencyEndpoint } from './CurrencyEndpoints/JsonCurrencyEndpoint';
import { XmlCurrencyEndpoint } from './CurrencyEndpoints/XmlCurrencyEndpoint';

@Injectable({
    providedIn: 'root'
})
export class CurrencyService {
    public currentEndpointIndex$$ = new BehaviorSubject<number>(0)
    public currentEndpoint$ = this.currentEndpointIndex$$.pipe(
        map(index => this.endpoints[index % this.endpoints.length])
    )
    private endpoints: ICurrencyEndpoint[] = [
        new JsonCurrencyEndpoint(this._http),
        new XmlCurrencyEndpoint(this._http)
    ]

    constructor(
        private _http: HttpClient
    ) {}

    getCurrency$() {
        return interval(1000 * 10)
            .pipe(
                startWith(0),
                switchMap(() => {
                    return this.currentEndpoint$.pipe(
                        switchMap((endpoint) => endpoint.getCurrency().pipe(catchError((e) => {
                            this.currentEndpointIndex$$.next(this.currentEndpointIndex$$.value + 1);
                            return EMPTY
                        })))
                    )
                })
            )
    }

}